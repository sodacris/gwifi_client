import _login
from _net import myget
import json
import logging

login = _login.login


def logout():
    logging.info("logging out...")
    url = "http://172.16.1.1/getApp.htm?action=logout"
    logging.info(json.loads(myget(url).text))


def get_test_content():
    url = "http://www.msftconnecttest.com/connecttest.txt"
    return repr(myget(url).text[:50])
