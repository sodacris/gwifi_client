from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import _conf as conf
import logging
from _driver import driver


def login():
    logging.info("authenticating...")

    # login page
    driver.get("http://172.17.1.1")
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="first_name"]'))
    )

    # fill form
    field1 = driver.find_element_by_xpath('//*[@id="first_name"]')
    field1.send_keys(conf.username)
    field2 = driver.find_element_by_xpath('//*[@id="first_password"]')
    field2.send_keys(conf.password)

    # login button
    btn = driver.find_element_by_xpath('//*[@id="first_button"]')
    btn.click()

    # wait
    WebDriverWait(driver, 10).until(
        lambda driver: driver.current_url != "http://login.giwifi.com.cn")

    logging.info("success")
    driver.close()
    return True


if __name__ == '__main__':
    login()
