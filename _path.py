from pathlib import Path

CURR_PATH = Path(__file__).resolve().parent

def mypath(filename):
    return Path.joinpath(CURR_PATH, filename)


CONFIG_PATH = mypath('config.ini')
LOG_PATH = mypath('geckodriver.log')