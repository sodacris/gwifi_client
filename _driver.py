from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import os
from pathlib import Path
from _path import LOG_PATH

# get driver
options = FirefoxOptions()
options.headless = True

if os.name == 'nt':
    DRIVER_PATH = Path.joinpath(Path(__file__).parent, 'driver',
                                'geckodriver.exe')

    driver = webdriver.Firefox(options=options,
                               executable_path=DRIVER_PATH,
                               service_log_path=LOG_PATH)
else:
    driver = webdriver.Firefox(options=options,
                               service_log_path=LOG_PATH)
