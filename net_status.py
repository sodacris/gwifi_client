import logging
import service
from _net import myget
CONNECT_SUCCESS = 1
REQUIRE_AUTH = 2
ERROR_HAPPENED = 3


def test_conn():
    url = "http://www.msftconnecttest.com/connecttest.txt"
    try:
        r = myget(url)
        logging.info("testing connection...")
        logging.info(service.get_test_content())
        if r.text == "Microsoft Connect Test":
            logging.info("connect success")
            return CONNECT_SUCCESS
        else:
            logging.info("authentication required")
            return REQUIRE_AUTH
    except Exception as e:
        print(e)
        logging.info("connection error")
        return REQUIRE_AUTH
