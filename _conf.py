import configparser
config = configparser.ConfigParser()
from _path import mypath

CONFIG = mypath('config.ini')
config.read(CONFIG)

username = config['AUTH']['username']
password = config['AUTH']['password']