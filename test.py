import logging
from net_status import ERROR_HAPPENED, test_conn, CONNECT_SUCCESS, REQUIRE_AUTH
import service

logging.basicConfig(
    format="%(asctime)s %(levelname)s %(message)s",
    datefmt="%Y-%m-%d %I:%M:%S %p",
    level=logging.INFO,
)

result = test_conn()
if(result == ERROR_HAPPENED):
    exit(2)
if(result == CONNECT_SUCCESS):
    # print("Connect success. logging out...")
    service.logout()
    if(test_conn() != REQUIRE_AUTH):
        logging.error("cannot logout")
        exit(-1)

service.login()
test_conn()
