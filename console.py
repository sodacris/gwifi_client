import os
import sys
import service
import logging
import subprocess
from net_status import test_conn, CONNECT_SUCCESS, REQUIRE_AUTH
import argparse

parser = argparse.ArgumentParser(description='GiWiFi login script')
group = parser.add_mutually_exclusive_group()
group.add_argument('-l', '--login', help='wifi login', action='store_true')
group.add_argument('-o', '--logout', help='wifi logout', action='store_true')
group.add_argument('-t', '--test', help='wifi test', action='store_true')
parser.add_argument('-v', '--verbose',
                    help='display more info', action='store_true')
args = parser.parse_args()

if args.verbose:
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %I:%M:%S %p",
        level=logging.INFO,
    )
else:
    logging.basicConfig(
        format='%(message)s', level=logging.INFO)


def get_wifi_name():
    name = ''
    if os.name == "nt":
        encoding = sys.stdout.encoding
        info = subprocess.run(
            ["netsh", "wlan", "show", "interfaces"], capture_output=True)
        info = info.stdout.decode(encoding=encoding, errors="ignore")
        for line in info.split('\r\n'):
            if "SSID" in line:
                name = line.split()[-1]
                break
    else:
        # iw wlan0 info|grep ssid|awk '{print $2}'
        wifi = subprocess.run('iw wlan0 info'.split(),
                              capture_output=True).stdout.decode()
        for line in wifi.split('\n'):
            if 'ssid' in line:
                name = line.split()[1]
                break
    logging.info(f'wifi name: {name}')
    return name


def is_gwifi():
    return 'GiWiFi' in get_wifi_name()


if not is_gwifi():
    exit(2)


if args.login:
    if test_conn() == REQUIRE_AUTH:
        service.login()

elif args.logout:
    if test_conn() == CONNECT_SUCCESS:
        service.logout()

elif args.test:
    print(service.get_test_content())
