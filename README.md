# GiWiFi login script

安装[firefox](https://www.mozilla.org/zh-CN/firefox/)

将`config.example.ini`复制为`config.ini`，在其中填入用户名和密码

powershell: 在`$profile`中加入：

```powershell
function glua {
  [CmdletBinding()]
  param (
      [Parameter()]
      [String]
      $op
  )
  # script location
  python "D:\code\python\gwifi_client\console.py" $op
}
```

zsh: set function
